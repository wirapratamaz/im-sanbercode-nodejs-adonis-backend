import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import ValueValidator from 'App/Validators/ValueValidator'

export default class ValuesController {
    public async kirim({request, response} : HttpContextContract){
        try {
            const newValues = await request.validate(ValueValidator)

              response.ok({
                message: newValues
              })
        } catch (error) {
            //menampung error
            response.badRequest(error.message)
        }
    }
}