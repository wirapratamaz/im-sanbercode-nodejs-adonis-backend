import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BookingValidator from 'App/Validators/BookingValidator'


export default class BookingsController {
    public async post({ request, response} : HttpContextContract){
        try {
            const newBooking = await request.validate(BookingValidator)

            response.ok({
                message : newBooking
            })
        } catch (error) {
            response.badRequest(error.messages)
        }
    }
}
