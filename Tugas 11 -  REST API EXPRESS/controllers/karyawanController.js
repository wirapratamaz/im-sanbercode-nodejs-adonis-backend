const { json } = require('express');
const res = require('express/lib/response');
const fs = require('fs');
const fsPromises = require('fs/promises');
const path = 'data.json';

//register karyawan
class karyawanController{
    static register(req, res) {
        fs.readFile(path, (err, data) => {
            if(err){
                res.status(400).json({
                    errors: 'tidak bisa membaca data'
                });
            } else {
                let existingData = JSON.parse(data);
                let { users } = existingData
                let { name, password, role } = req.body;
                let newKaryawan = { name, password, role, isLogin:false };
                //push data
                users.push(newKaryawan);
                let newData = {...existingData, users};

                fs.writeFile(path, JSON.stringify(newData), (err) => {
                    if(err){
                        res.status(400).json({
                            errors: 'tidak da[at menyimpan data',
                        })
                    } else {
                        res.status(201).json({
                            message: 'berhasil register',
                        });
                    }
                });
            }
        })
    }

    //Lihat semua karyawannya
    static findAll(req,res) {
        fs.readFile(path, (err, data) => {
            if(err){
                res.status(400).json({
                    errors: 'tidak dapat membaca data',
                });
            } else {
                let realData = JSON.parse(data);
                res.status(200).json({
                    message: 'berhasil membaca data',
                    data: realData.users,
                });
            }
        })
    }

    static login(req, res) {
        fsPromises
            .readFile(path)
            .then((data) => {
                let existingData = JSON.parse(data);
                let { users } = existingData;
                let { name, password } = req.body;
                let index = users.findIndex((employee) => employee.name == name);

                if(index == -1){
                    res.status(404).json({
                        errors: 'data tidak ditemukan',
                    });
                } else {
                    let karyawan = users[index];

                    if(karyawan.password == password){
                        karyawan.isLogin = true;

                        users.splice(index, 1, karyawan);
                        return fsPromises.writeFile(path, JSON.stringify(existingData));
                    } else {
                        res.status(400).json({
                            errors: 'password salah',
                        });
                    }
                }
            })
            .then(() => {
                res.status(200).json({
                    message: 'Berhasil Login',
                });
            })
            .catch((err) => {
                console.log(err);
            })
    }

    static addStudent(req, res) {
        fsPromises
          .readFile(path)
          .then((data) => {
            let existingData = JSON.parse(data);
            let { users } = existingData;
            let { name, kelas } = req.body;
            let newStudent = { name, kelas };
            let newTrainerData = { students: [] };
            newTrainerData.students.push(newStudent);
            let indexKr = users.findIndex((karyawan) => karyawan.role == 'admin');
            let admin = users[indexKr];
    
            if (indexKr == -1) {
              res.status(404).json({
                errors: 'admin tidak ditemukan'
              })
            } else {
              if (admin.role == 'admin' && admin.isLogin == true) {
                let indexTr = users.findIndex((trainer) => trainer.name === req.params.name);
                let trainer = users[indexTr];
                let newTrainer = { ...trainer, newTrainerData };
    
                users.splice(indexTr, 1, trainer);
    
                return fsPromises.writeFile(path, JSON.stringify(newTrainer));
              }
            }
          })
          .then(() => {
            res.status(201).json({
              message: 'berhasil add siswa'
            })
          })
          .catch((err) => {
            console.log(err);
          });
      }
    
}
