var express = require('express');
const { route } = require('.');
var router = express.Router();

//controller karyawan
const karyawanController = require('../controllers/karyawanController');

//controller lihat semua karyawan
router.get('/', karyawanController.findAll);

//controller POST register
router.post('/register', KaryawanController.register);
router.post('/login', karyawanController.login);
router.post('/:name/siswa', karyawanController.addStudent);

module.exports = router;