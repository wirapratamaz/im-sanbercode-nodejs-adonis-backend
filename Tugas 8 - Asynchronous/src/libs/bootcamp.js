import Employee from "./employee";
import fs from "fs";
import "core-js/stable";
const path = "data.json";
import fsPromises from "fs/promises";

class Bootcamp{
    static register(input){
        let [name, password, role] = input.split(",");
        fs.readFile(path, (err, data) => {
            //jika ada error
            if(err){
                console.log(err);
            }
            //jika datanya ada uabh jadi JSON
            let existingData = JSON.parse(data);
            //panggil class Employee
            let employee = new Employee(name, password, role);
            
            //masukkin datanya ke existingData
            existingData.push(employee);

            //masukkan data berdasarkan path
            //JSON.stringtify biar datanya kebaca
            fs.writeFile(path, JSON.stringify(existingData), (err) => {
                //jika ada error
                if(err){
                    console.log(err);
                } else {
                    //berhasilkan tampilan
                    console.log("Berhasil Register");
                }
            });
        });
    }

    static login(input){
        let [ name, password ] = input.split(",");

        //pakai promise
        //read file berdasarkan path
        fsPromises
            .readFile(path)
            .then((data) => {
                let employees = JSON.parse(data);

                //ambil index dari employee
                let indexEmp = employees.findIndex((emp) => emp._name === name);

                //index tidak ada minus 1 atau tidak dapat index
                if(indexEmp == -1){
                    console.log("Data tidak ditemukan");
                } else {
                    //jika datanya ditemukan dalam bentuk perorangan
                    let employee = employees[indexEmp];

                    //kondisi password
                    if(employee._password === password){
                        employee._isLogin = true;

                        employees.splice(indexEmp, 1, employee)
                        return fsPromises.writeFile(path, JSON.stringify(employees));
                    } else{
                        console.log("Password yang dimasukkan salah");
                    }
                }
            })
            .then(() => {
                console.log("Berhasil Login");
            })
            //kalo ada error
            .catch((err) => {
                console.log(err);
            })
    }

    static async addSiswa(input){
        try {
            let [ nama, trainer ] = input.split(",");
            let dataRead = await fsPromises.readFile(path);
            let indexData = JSON.parse(dataRead);
            let searchData = indexData.findIndex((t) => t._role == "admin");
            //cek login
            if(indexData[searchData]._isLogin === true){
                let trainerData = indexData.findIndex((t) => t._name == trainer);
                if(trainerData == -1){
                    console.log("Trainer tidak ditemukan");
                } else {
                    let trainer = indexData[trainerData];
                    let tempTrainer = (trainer._students = []);
                    let objTrainer = {};
                    objTrainer.name = nama;
                    tempTrainer.push(objTrainer);
                    console.log("Berhasil add siswa");
                    await fsPromises.writeFile(path, JSON.stringify(indexData));
                }
            }
        } catch (error) {
            console.log(error);
        }
    }
}

export default Bootcamp;