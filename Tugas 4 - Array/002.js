function balikKata(string) {
    var output ="";
    var panjangString = string.length;

    for (var index = panjangString-1; index >= 0; index--) {
        output += string[index];
    }

    return output;
}

console.log(balikKata("SanberCode"));
console.log(balikKata("racecar"));
console.log(balikKata("kasur rusak"));
console.log(balikKata("haji ijah"));
console.log(balikKata("I am Sanbers"));