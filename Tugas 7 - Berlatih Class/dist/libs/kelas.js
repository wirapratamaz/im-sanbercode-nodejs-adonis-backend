"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _bootcamp = _interopRequireDefault(require("./bootcamp"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Kelas = /*#__PURE__*/function () {
  function Kelas(name) {
    _classCallCheck(this, Kelas);

    this._name = name;
    this._classes = [];
  }

  _createClass(Kelas, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: "classes",
    get: function get() {
      return this._classes;
    } //function

  }, {
    key: "createClass",
    value: function createClass(name, level, instructor) {
      //metode kelas baru
      var newClass = new _bootcamp["default"](name, level, instructor); //push newClass ke constructor classes

      this._classes.push(newClass);
    } //function soal no 2

  }, {
    key: "insertStudent",
    value: function insertStudent(namaBootcamp, objStudent) {
      //cari kelas bootcamp
      var cariBootcamp = this._classes.find(function (t) {
        return t.name === namaBootcamp;
      });

      cariBootcamp.addStudent(objStudent);
    }
  }]);

  return Kelas;
}();

var _default = Kelas;
exports["default"] = _default;