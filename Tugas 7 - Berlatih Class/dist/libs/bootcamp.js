"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

var Bootcamp = /*#__PURE__*/function () {
  function Bootcamp(name, level, instructor) {
    _classCallCheck(this, Bootcamp);

    this._name = name;
    this._students = [];
    this._level = level;
    this._instructor = instructor;
  }

  _createClass(Bootcamp, [{
    key: "name",
    get: function get() {
      return this._name;
    },
    set: function set(name) {
      this._name = name;
    }
  }, {
    key: "instructor",
    get: function get() {
      return this._instructor;
    },
    set: function set(instructor) {
      this._instructor = instructor;
    }
  }, {
    key: "level",
    get: function get() {
      return this._level;
    },
    set: function set(level) {
      this._level = level;
    }
  }, {
    key: "students",
    get: function get() {
      return this._students;
    } //fungsi no 2

  }, {
    key: "addStudent",
    value: function addStudent(objStudent) {
      this._students.push(objStudent);
    }
  }]);

  return Bootcamp;
}();

var _default = Bootcamp;
exports["default"] = _default;