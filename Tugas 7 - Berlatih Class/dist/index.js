"use strict";

var _kelas = _interopRequireDefault(require("./libs/kelas"));

var _student = _interopRequireDefault(require("./libs/student"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

//inisialisasi object
var sanber = new _kelas["default"]("sanbercode");
sanber.createClass("Laravel", "beginner", "abduh");
sanber.createClass("React", "beginner", "abdul"); //* no 2

var names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"];
names.map(function (nama, index) {
  var newStud = new _student["default"](nama);
  var kelas = sanber.classes[index % 2].name;
  sanber.insertStudent(kelas, newStud);
}); // menampilkan data kelas dan student nya

sanber.classes.forEach(function (kelas) {
  console.log(kelas);
}); //ambil properti classes dari kelas.js
// console.log(sanber.classes);