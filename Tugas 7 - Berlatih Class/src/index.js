import Kelas from "./libs/kelas";
import Student from "./libs/student";

//inisialisasi object
const sanber = new Kelas("sanbercode");
sanber.createClass("Laravel", "beginner", "abduh");
sanber.createClass("React", "beginner", "abdul")

//* no 2
let names = ["regi", "ahmad", "bondra", "iqbal", "putri", "rezky"]
names.map((nama, index) => {
  let newStud = new Student(nama)
  let kelas = sanber.classes[index % 2].name
  sanber.insertStudent(kelas, newStud)
})
// menampilkan data kelas dan student nya
sanber.classes.forEach(kelas => {
  console.log(kelas)
});

//ambil properti classes dari kelas.js
// console.log(sanber.classes);