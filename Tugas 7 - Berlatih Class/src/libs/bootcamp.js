class Bootcamp{
    constructor(name, level, instructor){
        this._name = name
        this._students = []
        this._level = level
        this._instructor = instructor
    }
    
    get name(){
        return this._name;
    }

    set name(name){
        this._name = name; 
    }

    get instructor(){
        return this._instructor;
    }

    set instructor(instructor){
        this._instructor = instructor; 
    }

    get level(){
        return this._level;
    }

    set level(level){
        this._level = level; 
    }

    get students(){
        return this._students;
    }

    //fungsi no 2
    addStudent(objStudent){
        this._students.push(objStudent);
    }
}

export default Bootcamp;