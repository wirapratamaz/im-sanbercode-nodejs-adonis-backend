import Bootcamp from "./bootcamp";

class Kelas{
    constructor(name){
        this._name = name
        this._classes = []
    }

    get name(){
        return this._name;
    }

    set name(name){
        this._name = name;
    }

    get classes(){
       return this._classes;
    }

    //function
    createClass(name, level, instructor){
        //metode kelas baru
        let newClass = new Bootcamp(name, level, instructor)
        //push newClass ke constructor classes
        this._classes.push(newClass)
    }

    //function soal no 2
    insertStudent(namaBootcamp, objStudent){
        //cari kelas bootcamp
        let cariBootcamp = this._classes.find((t) => t.name === namaBootcamp);
        cariBootcamp.addStudent(objStudent);
    }
}

export default Kelas;