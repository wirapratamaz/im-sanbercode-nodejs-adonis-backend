function nilaiTertinggi(siswa) {
    var tempTertinggi = [];

    for (var index = 0; index < siswa.length; index++) {
        if(tempTertinggi.hasOwnProperty(siswa[index].class)){
            var kelas = siswa[index].class;
            
            if(siswa[index].score > tempTertinggi[kelas].score){
                tempTertinggi[kelas].name = siswa[index].name;
                tempTertinggi[kelas].score = siswa[index].score;
            }
        } else {
            var kelas = siswa[index].class;
            tempTertinggi = {
                ...tempTertinggi,
                [kelas]: {
                    name: siswa[index].name,
                    score: siswa[index].score,
                }
            }
        }
    }
    return tempTertinggi;
}

// TEST CASE
console.log(nilaiTertinggi([
    {
        name: 'Asep',
        score: 90,
        class: 'adonis'
    },
    {
        name: 'Ahmad',
        score: 85,
        class: 'vuejs'
    },
    {
        name: 'Regi',
        score: 74,
        class: 'adonis'
    },
    {
        name: 'Afrida',
        score: 78,
        class: 'reactjs'
    }
]));

console.log(nilaiTertinggi([
    {
        name: 'Bondra',
        score: 100,
        class: 'adonis'
    },
    {
        name: 'Putri',
        score: 76,
        class: 'laravel'
    },
    {
        name: 'Iqbal',
        score: 92,
        class: 'adonis'
    },
    {
        name: 'Tyar',
        score: 71,
        class: 'laravel'
    },
    {
        name: 'Hilmy',
        score: 80,
        class: 'vuejs'
    }
]));