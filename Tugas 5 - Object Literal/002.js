function naikAngkot(listPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var output = [];

    for (var index = 0; index < listPenumpang.length; index++) {
        var penumpangSekarang = listPenumpang[index];
        
        var objPenumpang = {
            penumpang: penumpangSekarang[0],
            naikDari: penumpangSekarang[1],
            tujuan: penumpangSekarang[2]
        }

        //pembayaran
        var bayar = (rute.indexOf(penumpangSekarang[2]) - rute.indexOf(penumpangSekarang[1])) * 2000;

        objPenumpang.bayar = bayar;

        output.push(objPenumpang);
    }
    return output;
}

console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]
));
console.log(naikAngkot([])); //[]
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]