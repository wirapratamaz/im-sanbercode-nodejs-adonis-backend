import {sapa, convert, checkScore, filterData} from './libs/soal';

const myArgs = process.argv.slice(2);
const command = myArgs[0];

//* Soal 1 Function Sapa
switch (command) {
  case 'sapa':
    let nama1 =  myArgs[1];
    console.log(sapa(nama1));
    break;
//*Soal 2 Konversi Object   
  case 'convert':
    const params = myArgs.slice(1);

    let [nama, domisili, umur] = params;

    console.log(convert(nama, domisili, umur));
    break;
//*Soal 3 Check Score
  case 'checkScore':
    let string = myArgs[1];
    console.log(checkScore(string));
    break;
//*Soal 4 
  case 'filterData':
    const data = myArgs[1];
    console.log(filterData(data));

    break;
  default:
    break;
}
