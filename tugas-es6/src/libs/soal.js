export const sapa = (nama) => {
    return `Halo selamat pagi, ${nama}`;
}

export const convert = (nama, domisili, umur) => {
    return {
        nama,
        domisili,
        umur
    }
}

export const checkScore = (data) => {
    const str = data.split(',');
    const obj = {};
    str.forEach(string => {
        const[key,value] = string.split(":");
        obj[key] = value;
    });
    return obj;
}

const data = [
    { name: "Ahmad", kelas: "adonis"},
    { name: "Regi", kelas: "laravel"},
    { name: "Bondra", kelas: "adonis"},
    { name: "Iqbal", kelas: "vuejs" },
    { name: "Putri", kelas: "Laravel" }
]

export const filterData = (kelas) => {
    for (let index = 0; index < data.length; index++) {
        return data.filter((el) => el['kelas'].toLowerCase().includes(kelas.toLowerCase()));
    }
}