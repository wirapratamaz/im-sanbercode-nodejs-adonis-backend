console.log("LOOPING PERTAMA");

i = 2; //start
while(i <= 20){
    var output = i + " - I love coding";
    console.log(output);
    i += 2;//i ditambah 2 
}

console.log("LOOPING KEDUA");

i = 20; //start
while (i >= 1) {
    var output = i + " - I will become a mobile developer";
    console.log(output);
    i -= 2;//i dikurang 2
}