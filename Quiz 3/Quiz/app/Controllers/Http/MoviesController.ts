import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class MoviesController {
  public async index({response}: HttpContextContract) {
    try {
        const dataMovies = await Database
            .from('movies')
            .select('*')
        response.ok({
            message: "Succesfully display data",
            data: dataMovies
        })
    } catch (error) {
        response.badRequest({
            message: "Failed to display data"
        })
    }
  }

  public async store({response, request}: HttpContextContract) {
    try {
      await Database
      .table('movies')
      .insert({
        title: request.input('title'),
        resume: request.input('resume'),
        release_date : request.input('release_date'),
        genre_id: request.input('genre_id')
      })
      response.ok({
        message: "Successfully save data"
      })
    } catch (error) {
      response.badRequest({
        message: "Failed to save data"
      })
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const showId = await Database
        .from('movies')
        .leftJoin('genres', 'movies.genre_id', 'genres.id')
        .select('movies.id', 'movies.title', 'movies.resume', 'movies.release_date', 'genres.name as genre')
        .where('movies.id', params.id)
      response.ok({
        message: "Successfully display ID",
        data: showId
      })
    } catch (error) {
      response.badRequest({
        message: "Failed to display ID"
      })
    }
  }

  public async update({response, request, params}: HttpContextContract) {
    try {
      await Database
        .from('movies')
        .where('id', params.id)
        .update({
          title: request.input('title'),
          resume: request.input('resume'),
          release_date : request.input('release_date'),
          genre_id: request.input('genre_id')
        })
        response.ok({
          message: "Successfully updated data",
        })
    } catch (error) {
      response.badRequest({
        message: "Failed to update data"
      })
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
        .from('movies')
        .where('id', params.id)
        .delete()
        response.ok({
          message: "Successfully deleted data",
        })
    } catch (error) {
      response.badRequest({
        message: "Failed to delete data"
      })
    }
  }
}
