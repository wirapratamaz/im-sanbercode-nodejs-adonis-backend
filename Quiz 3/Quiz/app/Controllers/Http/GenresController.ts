import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'

export default class GenresController {
  public async index({response}: HttpContextContract) {
    try {
      const dataGenres = await Database
        .from('genres')
        .select('*')
      response.ok({
        message: 'Sukses menampilkan data',
        data: dataGenres
      }) 
    } catch (error) {
      response.badRequest({
        message: 'Gagal menampilkan data'
      })
    }
  }

  public async store({response, request}: HttpContextContract) {
    try {
      await Database
      .table('genres')
      .insert({
        name: request.input('name'),
        movies_id: request.input('movies_id')
      })
    response.ok({
      message: 'Sukses save data'
    })  
    } catch (error) {
      response.badRequest({
        message: 'Gagal save data'
      })
    }
  }

  public async show({response, params}: HttpContextContract) {
    try {
      const showId = await Database
        .from('genres')
        .where('id', params.id)
        .first()
      response.ok({
        message: 'Data berhasil menampilkan ID ',
        data: showId
      })
    } catch (error) {
      response.badRequest({
        message: 'Data gagal menampilkan ID'
      })
    }
  }

  public async update({response, request, params}: HttpContextContract) {
    try {
      await Database
        .from('genres')
        .where('id', params.id)
        .update({
          name: request.input('name'),
          movies_id: request.input('movies_id')
      })
      response.ok({
        message: 'Sukses update data'
      })
    } catch (error) {
      response.badRequest({
        message: 'Gagal update data'
      })
    }
  }

  public async destroy({response, params}: HttpContextContract) {
    try {
      await Database
        .from('genres')
        .where('id', params.id)
        .delete()
        response.ok({
          message: 'Sukses hapus data'
        })  
    } catch (error) {
      response.badRequest({
        message: 'Gagal hapus data'
      })
    }
  }
}
